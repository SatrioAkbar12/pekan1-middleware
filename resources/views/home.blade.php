@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                    <br><br>
                    <a style="padding:5px; margin:2px; background-color: dodgerblue; color:white;" href="/route-1">Route 1</a>
                    <a style="padding:5px; margin:2px; background-color: dodgerblue; color:white;" href="/route-2">Route 2</a>
                    <a style="padding:5px; margin:2px; background-color: dodgerblue; color:white;" href="/route-3">Route 3</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
