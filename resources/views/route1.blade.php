@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h5>Ini adalah Route 1 yang hanya bisa diakses oleh SuperAdmin</h5>
                    <br>
                    <a style="padding:5px; margin:2px; background-color: crimson; color:white;" href="/home">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
