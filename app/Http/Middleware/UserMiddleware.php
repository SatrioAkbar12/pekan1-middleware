<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(URL::current() == route('route1') && Auth::user()->isSuperAdmin()){
            return $next($request);
        }
        else if(URL::current() == route('route2') && (Auth::user()->isSuperAdmin() || Auth::user()->isAdmin())){
            return $next($request);
        }
        else if(URL::current() == route('route3')){
            return $next($request);
        }
        abort(403);
    }
}
