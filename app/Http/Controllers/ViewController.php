<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function route1(){
        return view('route1');
    }
    public function route2(){
        return view('route2');
    }
    public function route3(){
        return view('route3');
    }
}
